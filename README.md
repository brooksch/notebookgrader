# Usage

**First, this must all be run in a Python 2 environment.**

1. Install docker, following these painful directions: https://docs.docker.com/engine/installation/linux/ubuntulinux/
2. Install the courseraprogramming script, in python 2, using pip. Details at https://github.com/coursera/courseraprogramming
3. Create a docker file that describes how the docker image is to be built. There is an example in Dockerfile.
4. Instantiate that into a docker container, by running "docker build -t grader ."
5. Run the docker container, thus executing the grader, using "docker run grader"

The coursera environment is somewhat different than a generic docker environment, so you need to test your examples with:

`courseraprogramming grade local grader .`

A common workflow is to build and test in one:

`docker build -t grader .; courseraprogramming grade local grader ./sources/`

Where the last parameter is the set of student submissions.

To deploy your docker image:

`courseraprogramming upload grader RZ1S0B0MEeacvQ6cODzg5Q 5otfC iAHnR`

To publish the changes you can go to the web ui for the assignment, or use:

`courseraprogramming publish RZ1S0B0MEeacvQ6cODzg5Q tHmgx`

Note: Despite a seemingly 1:1 mapping between course slugs and the course_id, you actually have to manually go here to find out what your course_id is (modify the URL as appropriate): [https://api.coursera.org/api/courses.v1?q=slug&slug=python-data-analysis] . Further, you have to go to the authoring page of the interface to find the part and item ID. While the part ID is plainly available, the details for the item ID are cryptically hidden in the extended help for the courseraprogramming upload command as "The id of the item to associate the grader. The easiest way to find the item id is by looking at the URL in the authoring web interface. It is the last part of the URL, and is a short UUID."

# To debug
To run the test cases, run `ipython tests.ipy`

To debug, do: `docker build -f Dockerfile.last -t grader .;docker run -it grader /bin/bash`


# References
[https://partner.coursera.help/hc/en-us/articles/205314475-Programming-Assignments?flash_digest=1749b20ffd19c34ec8124020122a1b653c4f5f95#header_3]

# Internal Notes:
- Deployment is in the form of courseraprogramming upload IMAGE COURSE ITEM PART
- Specific deployment for our first course:
    - second assignment is: `courseraprogramming upload grader RZ1S0B0MEeacvQ6cODzg5Q tHmgx OQsnr`
    - third assignment is: `courseraprogramming upload grader RZ1S0B0MEeacvQ6cODzg5Q zAr06 SL3fU`
    - fourth assignment is `courseraprogramming upload grader RZ1S0B0MEeacvQ6cODzg5Q Il9Fx WGlun`
- Specific publishing for our first course:
    - second assignment is: `courseraprogramming publish RZ1S0B0MEeacvQ6cODzg5Q tHmgx`
    - third assignment is: `courseraprogramming publish RZ1S0B0MEeacvQ6cODzg5Q zAr06`
    - fourth assignment is: `courseraprogramming publish RZ1S0B0MEeacvQ6cODzg5Q Il9Fx`
- You can't deploy in this way if there is an experiment, instead you need to `docker build -t grader .;docker run -it grader /bin/bash` 
followed by an export like `docker save grader > ../grader2016.11.19a.tar`

