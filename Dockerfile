# This uses anaconda as the base image
FROM continuumio/anaconda3
#for debug purposes
RUN apt-get update && apt-get install -y nano vim
RUN useradd -ms /bin/bash marker
# Make a directory where your files will be stored
RUN mkdir /home/marker/grader
WORKDIR /home/marker/grader
# Copy the grader script into the docker image
ADD sources/grader.ipy grader.ipy
# Copy the solution files and data files into the docker image
ADD sources/*.ipynb ./
ADD sources/*.csv ./
ADD sources/*.xls ./
ADD sources/*.xlsx ./
ADD sources/*.txt ./
# Shut off colors for ipython
RUN mkdir /home/marker/.ipython
RUN mkdir /home/marker/.ipython/profile_default
ADD sources/ipython_config.py /home/marker/.ipython/profile_default/ipython_config.py
#add in a testing file for assignment 4
#ADD sources/assignment4_correct_solution.ipynb /shared/submission/assignment4_student_solution.ipynb
# Setup Permissions
RUN chmod a+rwx -R /home/marker/
# Change to user
USER marker
# To test locally, run the following: docker build -t grader .;docker run -it grader /bin/bash
# then run ipython3 grader.ipy
# Verify that assignments work correctly with docker build -t grader .; docker run -i -t grader
# Uncomment all of the below to test an assignment
#USER root
#RUN mkdir /shared
#RUN mkdir /shared/submission
#RUN chmod a+rwx -R /shared/submission
#USER marker# Uncomment the assignment below which you wish to test
#COPY sources/Assignment_2.ipynb /shared/submission/assignment2_student_solution.ipynb
#COPY sources/Assignment_2.py /shared/submission/assignment2_student_solution.ipynb
#COPY sources/assignment2_correct_solution.ipynb /shared/submission/assignment2_student_solution.ipynb
#COPY sources/Assignment_3.ipynb /shared/submission/assignment3_student_solution.ipynb
#COPY sources/Assignment_3.py /shared/submission/assignment3_student_solution.ipynb
#COPY sources/assignment3_correct_solution.ipynb /shared/submission/assignment3_student_solution.ipynb
#COPY sources/Assignment_4.ipynb /shared/submission/assignment4_student_solution.ipynb
#COPY sources/Assignment_4.py /shared/submission/assignment4_student_solution.ipynb
#COPY sources/assignment4_correct_solution.ipynb /shared/submission/assignment4_student_solution.ipynb
#COPY sources/assignment3_test_correct_solution_shuffle.ipynb /shared/submission/assignment3_student_solution.ipynb
#COPY sources/assignment2_student_solution_doesnt_run.ipynb /shared/submission/assignment2_student_solution.ipynb
#COPY sources/assignment4_student_solution_file_open.ipynb /shared/submission/assignment4_student_solution.ipynb
#RUN ["/opt/conda/bin/ipython","grader.ipy"]
# Uncomment this if you want to lock into the container
#RUN ["/bin/bash"]


#USER root
#RUN chmod a+rwx -R /home/marker/
#RUN mkdir /shared
#RUN mkdir /shared/submission
#RUN chmod a+rwx -R /shared/submission
#USER marker
#COPY sources/Assignment_2.ipynb /shared/submission/assignment2_student_solution.ipynb
#RUN ["/bin/bash"]

ENTRYPOINT ["/opt/conda/bin/ipython","grader.ipy"]